using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MASTERSCRIPT : MonoBehaviour
{

    // CHALLENGE 1 
    public TableHasCorrectBox RedTable;
    public TableHasCorrectBox BlueTable;
    public TableHasCorrectBox YellowTable;

    public Transform Challenge1CompleteDoor;
    public Renderer Challenge1DoorLight;

    public bool Challenge1 = false;



    //CHALLENGE 2
    public string actualPassword; // This is the password that should match 
    private string enteredPassword; // this is the password that will be entered by the player 
    private int enteredDigits; // this will keep track of the no. of digits player has entered  

    public TextMeshProUGUI keypadText;


    //CHALLENGE DOORS
    public Door door1;
    public Door door2;



    private void Start()
    {
        Challenge1DoorLight.material.color = Color.red; // SETS THE STARTING COLOR OF THE DOOR LIGHT AS RED 
        enteredDigits = 0;
        keypadText.text = "****";
    }

   
    public void Challenge1Complete()
    {
        //INSERT CODE FOR CHALLENGE1 DOOR COMPLETE CODE 
        //Debug.Log("Button Working");

        if (Challenge1)
        {
            // INSERT ACTUAL CODE FOR OPENING DOOR 
            //Debug.Log("Button making door go brrrr");

            door1.OpenDoor();
        }

    }
    
    public void ChangeDoorLight()
    {
        if (RedTable.boolCorrect && BlueTable.boolCorrect && YellowTable.boolCorrect)
        {
            Challenge1DoorLight.material.color = Color.green;
            Challenge1 = true;
        }
    }

    public void inputNumber(string number)
    {
        if(enteredDigits < 4)
        {
            enteredPassword += number;
            UpdateKeypad();
            enteredDigits++;
        }
    }

    public void confirmPasswordonKeypad()
    {
        if(enteredPassword == actualPassword)
        {
            //CHALLENGE COMPLETE DO SOMETHING 
            keypadText.text = "CORRECT";

            Challenge2Complete();
        }
        else
        {
            //RESET 
            enteredDigits = 0;
            enteredPassword = "";
            UpdateKeypad();
        }
    }

    public void UpdateKeypad()
    {
        keypadText.text = enteredPassword ;
    }

    public void Challenge2Complete()
    {
        //Enter code for Challenge 2 to be completed 
        door2.OpenDoor();
    }
}
