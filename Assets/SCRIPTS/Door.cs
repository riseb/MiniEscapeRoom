using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool isMoveDoor = false;
    public float MaxHeight = 10f;
    public float MoveSpeed = 10f;
    
    public void OpenDoor()
    {
        isMoveDoor = true;
    }

    private void Update()
    {
        if (isMoveDoor)
        {
            if (transform.position.y < MaxHeight)
                transform.Translate(Vector3.up * MoveSpeed * Time.deltaTime);
        }
        

    }
}
