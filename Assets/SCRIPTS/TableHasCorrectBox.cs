using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableHasCorrectBox : MonoBehaviour
{
    public bool boolCorrect;
    public string stringColor;

    public MASTERSCRIPT masterScript;
    private void Start()
    {
        masterScript = FindAnyObjectByType<MASTERSCRIPT>();
    }
    // Start is called before the first frame update
    private void OnCollisionStay(Collision collision)
    {
        //Debug.Log(collision.gameObject.tag +" = " + this.gameObject.tag);
        if (collision.gameObject.tag == this.gameObject.tag)
        {
            boolCorrect = true;
            masterScript.ChangeDoorLight();
        }
        else
            boolCorrect = false;
        
    }
}
